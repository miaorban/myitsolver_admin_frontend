// Import ES6 Promise
import 'es6-promise/auto'

// Import System requirements
import Vue from 'vue'
import VueRouter from 'vue-router'

// Set default header
import axios from 'axios'

var VueCookie = require('vue-cookie')

import { sync } from 'vuex-router-sync'
import routes from './routes'
import store from './store'

// Import api library
import helpers from '../src/library/authApi'
import Request from '../src/library/requestApi'

const plugin = {
  install () {
    Vue.helpers = helpers
    Vue.prototype.$helpers = helpers
  }
}
const requestApiLib = {
  install () {
    Vue.helpers = Request
    Vue.prototype.$Request = Request
  }
}

// Import Views - Top level
import AppView from './components/App.vue'

// Use the plugins
Vue.use(VueCookie)
Vue.use(plugin)
Vue.use(requestApiLib)
Vue.use(VueRouter)

// Send session id in header with all requests
axios.defaults.headers.common['x-access-token'] = Vue.cookie.get('SESSION_ID')

// Global interceptor to handle logout when authentication for x-access-token fails
axios.interceptors.response.use(
function(response) { return response },
function(error) {
  if (error.response.status === 401) {
    router.push('/login')
  }
})

// Routing logic
var router = new VueRouter({
  routes: routes,
  mode: 'history',
  linkExactActiveClass: 'active',
  scrollBehavior: function(to, from, savedPosition) {
    return savedPosition || { x: 0, y: 0 }
  }
})

// Some middleware to help us ensure the user is authenticated.
router.beforeEach((to, from, next) => {
  if (
    to.matched.some(record => record.meta.requiresAuth) &&
    (router.app.$cookie.get('SESSION_ID') === null || router.app.$store.state.token === 'null')
  ) {
    next({
      path: '/login',
      query: { redirect: to.fullPath }
    })
  } else {
    next()
  }
})

// Log out if not authenticated
// router.afterEach((to, from) => {
//   console.log(from)
//   if (from.path === '/logout') {
//     router.push('/login')
//   }
// })

sync(store, router)

// Start out app!
// eslint-disable-next-line no-new
new Vue({
  el: '#root',
  router: router,
  store: store,
  render: h => h(AppView)
})

// change this. demo
window.bugsnagClient = window.bugsnag('02fe1c2caaf5874c50b6ee19534f5932')
window.bugsnagClient.use(window.bugsnag__vue(Vue))
