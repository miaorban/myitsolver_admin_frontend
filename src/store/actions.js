import axios from 'axios'

export default {

  login({commit}, user) {
    commit('TOGGLE_LOADING')

    return new Promise((resolve, reject) => {
      axios.post('http://localhost:3000/auth/login', user)
      .then((sessionid) => {
        if (sessionid.data.success) {
          commit('SET_TOKEN', sessionid.data.session)
          axios.defaults.headers.common['x-access-token'] = sessionid.data.session
          // axios.defaults.headers.post['header1'] = 'value'
        } else {
          // this.response = sessionid.msg
        }
        resolve(sessionid.data)
      })
      .catch(error => {
        commit('TOGGLE_LOADING')
        reject(error)
      })
    })
  }
}
