import axios from 'axios'

export default {
  LoginUser (user) {
    return new Promise((resolve, reject) => {
      axios.post('http://localhost:3000/auth/login', user)
      .then((response) => {
        resolve(response.data)
      })
      .catch(error => reject(error))
    })
  },

  LogoutUser () {
    return new Promise((resolve, reject) => {
      // console.log('user: ', user)
      axios.get('http://localhost:3000/auth/logout')
      .then((response) => {
        resolve(response.data)
      })
      .catch(error => reject(error))
    })
  }
}
