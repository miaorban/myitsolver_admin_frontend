import axios from 'axios'

export default {
  collect () {
    return new Promise((resolve, reject) => {
      axios.get('http://localhost:3000/request/collect/basic')
        .then((response) => {
          resolve(response.data)
        })
        .catch(error => reject(error))
    })
  },

  getDetails (requestId) {
    return new Promise((resolve, reject) => {
      axios.get(`http://localhost:3000/request/find/${requestId}`)
        .then((response) => {
          resolve(response.data)
        })
        .catch(error => reject(error))
    })
  }
}
