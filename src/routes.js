
// import DashView from './components/Dash.vue'
import LoginView from './components/Login.vue'
import NotFoundView from './components/404.vue'

// Import Views - Dash
import TablesView from './components/views/Tables.vue'
import Logout from './components/views/Logout.vue'

// Routes
const routes = [
  {
    path: '/login',
    component: LoginView
  },
  {
    path: '/',
    component: TablesView,
    meta: {requiresAuth: true}
  },
  {
    path: '/logout',
    component: Logout,
    meta: {requiresAuth: true}
  },
  {
    // not found handler
    path: '*',
    component: NotFoundView
  }
]

export default routes
